import { Component } from "react";
import "./App.css";
import ProductList from "./components/ProductList";
import Sale from "./components/Sale";
import "./components/style.css";
import { FaStore } from "react-icons/fa";
import { products } from "./components/helper";
class App extends Component {
  render() {
    return (
      <div className="wrapper">
        <header>
          <FaStore className="icon" />
          <h1>Mercearia</h1>
        </header>

        {products.map((item) => {
          if (item.discountPercentage) {
            return (
              <div className="container">
                <ProductList
                  productsName={item.name}
                  productsPrice={`R$ ${item.price}`}
                />
                <Sale
                  title="Desconto:"
                  productsDisconts={
                    item.discountPercentage +
                    "%" +
                    "  Valor com Desconto:  " +
                    (`${item.price}` -
                      `${item.price}` * (`${item.discountPercentage}` / 100))
                  }
                />
              </div>
            );
          } else {
            return (
              <ProductList
                productsName={item.name}
                productsPrice={"R$" + item.price}
              />
            );
          }
        })}
      </div>
    );
  }
}

export default App;
