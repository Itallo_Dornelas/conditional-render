import { Component } from "react";

class ProductList extends Component {
  render() {
    return (
      <div>
        <h2>
          {this.props.productsName} {this.props.productsPrice}
        </h2>
      </div>
    );
  }
}

export default ProductList;
