import { Component } from "react";

class Sale extends Component {
  render() {
    return (
      <div className="sales">
        <h3>{this.props.title}</h3>
        <h3 className="discont">{this.props.productsDisconts}</h3>
      </div>
    );
  }
}

export default Sale;
